package ppa.labs.aspectj.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectLogger {
	private static Logger LOGGER = LogManager.getLogger(AspectLogger.class);
	
	@Around("@annotation(Logged)")
	public void logEntity (final ProceedingJoinPoint joinPoint) {
		try {
			joinPoint.proceed();
		} catch (Throwable e) {
			LOGGER.error(String.format("an error occured : ", e.getMessage()), e);
		}
		
	}
}
