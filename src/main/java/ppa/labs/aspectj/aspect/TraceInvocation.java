package ppa.labs.aspectj.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TraceInvocation {
	private static Logger LOGGER = LogManager.getLogger(TraceInvocation.class);
	private int order;

	@Around("traceInvocationPointcut()")
	public Object afficherTrace(final ProceedingJoinPoint joinpoint)
			throws Throwable {
		String nomMethode = joinpoint.getTarget().getClass().getSimpleName() + "."
				+ joinpoint.getSignature().getName();

		final Object[] args = joinpoint.getArgs();
		final StringBuffer sb = new StringBuffer();
		sb.append(joinpoint.getSignature().toString());
		sb.append(" avec les parametres : (");

		for (int i = 0; i < args.length; i++) {
			sb.append(args[i]);
			if (i < args.length - 1) {
				sb.append(", ");
			}
		}
		sb.append(")");
		LOGGER.info("Debut methode : " + sb);
		Object obj = null;
		try {
			obj = joinpoint.proceed();
		} finally {
			LOGGER.info("Fin methode : " + nomMethode + " retour=" + obj);
		}
		return obj;
	}
	
	@Pointcut("execution(* ppa.labs.aspectj.aspect.service.*ServiceImpl.*(..))")
	  public void traceInvocationPointcut() {
	  }
}
