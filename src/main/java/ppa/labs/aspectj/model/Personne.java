package ppa.labs.aspectj.model;

import java.time.LocalDate;

public class Personne {
	private String firstName;
	private String lastName;
	private LocalDate birthday;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	
	@Override
	public String toString () {
		return String.format(
				""
				+ "firstName = %s\n"
				+ "lastName = %s\n"
				+ "birthday = %s\n"
				,
				this.firstName,
				this.lastName,
				(this.birthday != null) ? this.birthday.toString() : null
				);
	}
}
