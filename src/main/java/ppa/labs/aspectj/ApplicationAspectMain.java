package ppa.labs.aspectj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationAspectMain {
	public static void main(String... args) {
	    SpringApplication.run(ApplicationAspectMain.class, args);
	  }
}
