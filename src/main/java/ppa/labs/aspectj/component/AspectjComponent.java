package ppa.labs.aspectj.component;

import org.springframework.stereotype.Component;

import ppa.labs.aspectj.annotation.Logged;
import ppa.labs.aspectj.model.Personne;
import ppa.labs.aspectj.service.PersonneService;

@Component
public class AspectjComponent {
	
	private PersonneService personneService;
	
	public AspectjComponent (PersonneService personneService) {
		this.personneService = personneService;
	}

	@Logged
	public void outPersonne (final Personne personne) {
		personneService.afficher(personne);
	}
}
