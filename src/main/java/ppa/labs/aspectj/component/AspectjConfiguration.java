package ppa.labs.aspectj.component;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ppa.labs.aspectj.service.PersonneService;
import ppa.labs.aspectj.service.PersonneServiceImpl;

@Configuration
public class AspectjConfiguration {
	
	@Bean
	public PersonneService personneService () {
		return new PersonneServiceImpl();
	}
}
