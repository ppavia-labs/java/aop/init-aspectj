package ppa.labs.aspectj.service;

import ppa.labs.aspectj.model.Personne;

public class PersonneServiceImpl implements PersonneService {

	@Override
	public void afficher(final Personne personne) {
		System.out.println(personne.toString());
	}

	@Override
	public void ajouter(final Personne personne) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
