package aspectjppa.labs.aspectj;

import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ppa.labs.aspectj.component.AspectjComponent;
import ppa.labs.aspectj.component.AspectjConfiguration;
import ppa.labs.aspectj.model.Personne;
import ppa.labs.aspectj.service.PersonneService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=AspectjConfiguration.class)
public class AspectjComponentTest {
	
	@Autowired
	private PersonneService personneService;

	@Test
	public void outPersonneTest () {
		AspectjComponent aspectComponent = new AspectjComponent(personneService);
		Personne personne = new Personne();
		personne.setFirstName("john");
		personne.setLastName("Doe");
		personne.setBirthday(LocalDate.of(1984, Month.AUGUST, 8));
		aspectComponent.outPersonne(personne);
		assertNotNull(personne);
	}
}
